FROM nginx

ARG	WORKPLACE="/usr/share/nginx/html"

RUN	apt-get update -y && \
	apt-get install -q -y nginx git vim && \
	apt-get autoclean -y && \
	apt-get clean -y && \
	cd $WORKPLACE && \
	rm -Rf $WORKPLACE/index.html

WORKDIR	$WORKPLACE

COPY index.html .

EXPOSE	80/tcp
EXPOSE	80/udp
EXPOSE	443/tcp
EXPOSE	443/udp

CMD	["nginx", "-g", "daemon off;"]